package de.waldemarlehner.particles;

import processing.core.PApplet;
import processing.core.PVector;

import java.util.LinkedList;
import java.util.List;

public class Main extends PApplet
{
    // Settings
    private final ParticleEmitter emitter = ParticleEmitter.FireConfig(30, 60);
    private final int spawnRate = 2;
    // end

    private int spawnCounter = 0;
    private final LinkedList<Particle> renderables = new LinkedList<>();

    private final List<Particle> itemsToRemove = new LinkedList<>();


    @Override
    public void settings()
    {
        super.size( 512, 512 );
    }

    @Override
    public void setup()
    {
        super.noStroke();
    }

    @Override
    public void draw()
    {
        super.background( 0xAAAAAA );
        if(spawnCounter >= spawnRate) {
            spawnCounter = 0;
            var particle = this.emitter.createParticle( new PVector(mouseX, mouseY, 0) );
            particle.getParticleTTLExpiredListener().subscribe( itemsToRemove::add );
            renderables.addLast( particle );
        }

        for(var particle : this.renderables)
        {
            particle.draw( this );
        }

        if(this.itemsToRemove.size() > 0) {
            this.itemsToRemove.forEach( this.renderables::remove );
            this.itemsToRemove.clear();
        }
        spawnCounter++;
    }

    // Program entry point
    public static void main( String[] args )
    {
        PApplet.main( Main.class, args );
    }

}
