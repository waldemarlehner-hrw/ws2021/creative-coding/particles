package de.waldemarlehner.particles;

import de.waldemarlehner.javaListenerUtils.Action;
import de.waldemarlehner.javaListenerUtils.Func;
import de.waldemarlehner.javaListenerUtils.ITopic;
import de.waldemarlehner.javaListenerUtils.TopicImpl;
import processing.core.PApplet;
import processing.core.PVector;

import java.awt.*;

public class Particle
{
    private final int timeToLiveFrames;
    private final Func.OneArg<Float, Color> colorOverLifeCycleFunction;
    private final Func.OneArg<Float, Float> sizeOverLifeCycleFunction;
    private final Func.OneArg<Float, PVector> positionOverLifeCycleFunction;
    private final PVector spawnPosition;

    private final TopicImpl<Action.OneArg<Particle>> particleDeadListener = new TopicImpl<>();

    private int frameCounter = 0;

    public Particle(
            int timeToLiveFrames,
            Func.OneArg<Float, Color> colorOverLifeCycleFunction,
            Func.OneArg<Float, Float> sizeOverLifeCycleFunction,
            Func.OneArg<Float, PVector> positionOverLifeCycleFunction,
            PVector spawnPosition
    )
    {
        this.timeToLiveFrames = timeToLiveFrames;
        this.colorOverLifeCycleFunction = colorOverLifeCycleFunction;
        this.sizeOverLifeCycleFunction = sizeOverLifeCycleFunction;
        this.positionOverLifeCycleFunction = positionOverLifeCycleFunction;
        this.spawnPosition = spawnPosition;
    }

    public void draw( PApplet applet )
    {
        var fraction = (float)frameCounter / (float)timeToLiveFrames;
        var position = this.positionOverLifeCycleFunction.invoke( fraction ).add( this.spawnPosition );
        var scale = this.sizeOverLifeCycleFunction.invoke(fraction);
        var color = this.colorOverLifeCycleFunction.invoke( fraction );

        applet.fill( color.getRGB() );
        applet.rect( position.x, position.y,  scale,  scale );

        this.frameCounter++;
        if(this.frameCounter > this.timeToLiveFrames)
        {
            this.particleDeadListener.fireListeners( l -> l.invoke( this ) );
        }
    }

    public ITopic<Action.OneArg<Particle>> getParticleTTLExpiredListener()
    {
        return this.particleDeadListener;
    }
}
