package de.waldemarlehner.particles;

import de.waldemarlehner.javaListenerUtils.Func;
import processing.core.PVector;

import java.awt.*;
import java.util.Random;

public class ParticleEmitter
{
    private final Func.OneArg<Random, Func.OneArg<Float, PVector>> positionFunctionFactory;
    private final Func.OneArg<Random, Func.OneArg<Float, Float>> sizeFunctionFactory;
    private final Func.OneArg<Random, Func.OneArg<Float, Color>> colorFunctionFactory;
    private final Func.OneArg<Random, Integer> timeToLiveFunction;
    private final Random random;

    public ParticleEmitter(
            Func.OneArg<Random, Func.OneArg<Float, PVector>> positionFunctionFactory,
            Func.OneArg<Random, Func.OneArg<Float, Float>> sizeFunctionFactory,
            Func.OneArg<Random, Func.OneArg<Float, Color>> colorFunctionFactory,
            Func.OneArg<Random, Integer> timeToLiveFunction,
            Random random
    ) {
        this.positionFunctionFactory = positionFunctionFactory;
        this.sizeFunctionFactory = sizeFunctionFactory;
        this.colorFunctionFactory = colorFunctionFactory;
        this.timeToLiveFunction = timeToLiveFunction;
        this.random = random;
    }

    public Particle createParticle(PVector position) {

        return new Particle(
            this.timeToLiveFunction.invoke( random ),
            this.colorFunctionFactory.invoke( random ),
            this.sizeFunctionFactory.invoke( random ),
            this.positionFunctionFactory.invoke( random ),
            position
        );
    }

    public static ParticleEmitter FireConfig(int ttlMin, int ttlMax)
    {
        Func.OneArg<Random, Integer> timeToLiveFunction = (Random rand) -> (int)(rand.nextFloat() * (ttlMax - ttlMin)) + ttlMin;

        Func.OneArg<Random, Func.OneArg<Float, Float>> sizeFunctionFactory = (Random rand) -> {
            var maxSize = 30 + 30 * rand.nextFloat();
            return (percentagePlayed) -> {
                if(percentagePlayed < 0) {
                    percentagePlayed = 0f;
                }
                else if(percentagePlayed > 1) {
                    percentagePlayed = 1f;
                }

                return (float)(Math.sin( Math.PI * percentagePlayed ) * maxSize);
            };
        };

        Func.OneArg<Random, Func.OneArg<Float, Color>> colorFunctionFactory = (Random rand) -> {
            var startingHueMin = 50;
            var startingHueMax = 60;
            var startingValueMin = 0.9f;
            var startingValueMax = 1f;

            var endingHueMin = 0;
            var endingHueMax = 15;
            var endingValueMin = 0.6f;
            var endingValueMax = 0.8f;

            var startingHue = rand.nextFloat() * (startingHueMax - startingHueMin) + startingHueMin;
            var endingHue = rand.nextFloat() * (endingHueMax - endingHueMin) + endingHueMin;
            var startingValue = rand.nextFloat() * (startingValueMax - startingValueMin) + startingValueMin;
            var endingValue = rand.nextFloat() * (endingValueMax - endingValueMin) + endingValueMin;

            return (percentagePlayed) -> {
                var hue = startingHue + percentagePlayed * (endingHue - startingHue);
                var value = startingValue + percentagePlayed * (endingValue - startingValue);
                var saturation = 1f;

                return Color.getHSBColor( hue / 360f, saturation, value );
            };
        };

        Func.OneArg<Random, Func.OneArg<Float, PVector>> positionFunctionFactory = (Random rand) -> {
            float xOffsetMax = 200 * (rand.nextFloat() - 0.5f);

            // see https://www.desmos.com/calculator/vrjmyfdbxw
            final float b = 4f + 5f * rand.nextFloat();
            final float c = 15 + rand.nextFloat() * 15f;

            return (percentagePlayed) -> {
                var dX = xOffsetMax * percentagePlayed;
                var main = (percentagePlayed * c) - b;
                var dY = (main*main) - (b * b);
                return new PVector(dX, dY, 0f);
            };
        };

        return new ParticleEmitter( positionFunctionFactory, sizeFunctionFactory, colorFunctionFactory, timeToLiveFunction, new Random() );
    }

    public static ParticleEmitter SmokeConfig(int ttlMin, int ttlMax)
    {
        Func.OneArg<Random, Integer> timeToLiveFunction = (Random rand) -> (int)(rand.nextFloat() * (ttlMax - ttlMin)) + ttlMin;

        Func.OneArg<Random, Func.OneArg<Float, Float>> sizeFunctionFactory = (Random rand) -> {
            var maxSize = 30 + 30 * rand.nextFloat();
            return (percentagePlayed) -> {
                if(percentagePlayed < 0) {
                    percentagePlayed = 0f;
                }
                else if(percentagePlayed > 1) {
                    percentagePlayed = 1f;
                }

                return (float)(Math.sin( Math.PI * percentagePlayed ) * maxSize);
            };
        };

        Func.OneArg<Random, Func.OneArg<Float, Color>> colorFunctionFactory = (Random rand) -> {
            var startingHueMin = 0;
            var startingHueMax = 0;
            var startingValueMin = 0.2f;
            var startingValueMax = 0.5f;

            var endingHueMin = 0;
            var endingHueMax = 0;
            var endingValueMin = 0.5f;
            var endingValueMax = 0.9f;

            var startingHue = rand.nextFloat() * (startingHueMax - startingHueMin) + startingHueMin;
            var endingHue = rand.nextFloat() * (endingHueMax - endingHueMin) + endingHueMin;
            var startingValue = rand.nextFloat() * (startingValueMax - startingValueMin) + startingValueMin;
            var endingValue = rand.nextFloat() * (endingValueMax - endingValueMin) + endingValueMin;

            return (percentagePlayed) -> {
                var hue = startingHue + percentagePlayed * (endingHue - startingHue);
                var value = startingValue + percentagePlayed * (endingValue - startingValue);
                var saturation = 0f;

                return Color.getHSBColor( hue / 360f, saturation, value );
            };
        };

        Func.OneArg<Random, Func.OneArg<Float, PVector>> positionFunctionFactory = (Random rand) -> {
            float xOffsetMax = 200 * (rand.nextFloat() - 0.5f);

            // see https://www.desmos.com/calculator/vrjmyfdbxw
            final float b = 4f + 5f * rand.nextFloat();
            final float c = 15 + rand.nextFloat() * 15f;

            return (percentagePlayed) -> {
                var dX = xOffsetMax * percentagePlayed;
                var main = (percentagePlayed * c) - b;
                var dY = -(main*main) + (b * b);
                return new PVector(dX, dY, 0f);
            };
        };

        return new ParticleEmitter( positionFunctionFactory, sizeFunctionFactory, colorFunctionFactory, timeToLiveFunction, new Random() );
    }

}
